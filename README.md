## Git checkout and compile Qemu 3.1.0 with following 2 patches
1. 1280x720 default console window resolution
2. egl-headless mode fix

### Git clone
```
$ git clone https://gitlab.com/barry.smith/qemu.git -b qemu_3.1.0
```

### Configure and Build Qemu
```
$ cd qemu/src
$ mkdir build
$ cd build

// Set env PKG_CONFIG_PATH to allow configuration script to find libvirglrenderer
$ export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
$ ../configure --target-list=aarch64-softmmu --enable-gtk --enable-kvm --enable-virglrenderer --enable-opengl
$ make -j

// Do a 'make install' to install qemu-sytem-aarch64 to /usr/local/bin
$ make install
```